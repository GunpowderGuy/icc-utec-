#https://www.hackerrank.com/semana05-jueves-s1-l6-sesion2-9amprof-mariano-melgar

def lineal(lista):
    return lista == list(range(lista[0],lista[0]+len(lista)))

def consecutivo(lista):
    if lineal(lista) == True :
        return "Son consecutivos"
    else : 
        return "No son consecutivos"
    
def tolista(cadena):
   return list(map(int,list(cadena)))

print(consecutivo(tolista(input())))