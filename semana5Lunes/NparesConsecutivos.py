# #https://www.hackerrank.com/contests/semana05-lunes-s1-l6-sesion1-7amprof-mariano-melgar/challenges/n-pare#s-consecutivos/submissions/code/1310363459

n = int(input())

def pares(n,count):
    if count <= n :
        print(count*2)
        pares(n,count+1)
    else :
        return 0

if n <= 0 or n >= 100:
    print("Invalid input")
else:
    pares(n,1)