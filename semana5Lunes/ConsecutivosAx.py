x = int(input())
n = int(input())

def consecutivos(x,n,count):
    if count < n :
        print(x+count+1)
        consecutivos(x,n,count+1)
    else :
        return 0

if n <= 0 or n >= 100 or x >= 1000 or x <= 0:
    print("Invalid input")
else:
    consecutivos(x,n,0)