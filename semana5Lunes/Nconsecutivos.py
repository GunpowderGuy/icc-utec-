n = int(input())

def consecutivos(n,count):
    if count < n :
        print(count+1)
        consecutivos(n,count+1)
    else :
        return 0

if n <= 0 or n >= 100:
    print("Invalid input")
else:
    consecutivos(n,0)