import operator as op
from functools import reduce

divisores = lambda numero : filter(lambda divisor : numero % divisor == 0 , range(1,numero))

numeroPerfecto = lambda n : n == reduce(op.add,divisores(n))

print(int(numeroPerfecto(int(input()))))