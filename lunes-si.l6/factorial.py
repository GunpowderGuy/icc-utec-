from functools import reduce
import operator as op

def factorial(n):
    return reduce(op.mul,range(1,n+1))

def factor(n):
    return reduce(op.add,map(factorial,range(1,n+1)))

print(factor(int(input())))

# https://www.hackerrank.com/set-de-problemas-2-lunes-s1-l6-sesion1-7amprof-mariano-melgar
