import operator as op
from functools import reduce 

numeroConsecutivos = lambda cadena : reduce(op.add,map(lambda x : cadena[x] == cadena[x+1]  ,range(len(cadena)-1))) 

palabras = lambda cadena : len(list(filter(lambda x : x == " ", cadena))) - numeroConsecutivos(cadena) + 1 

otrosCaracteres = lambda cadena : len(cadena) - palabras(cadena) + 1 - numeroConsecutivos(cadena)

respuesta = lambda cadena : "p:"+str(palabras(cadena))+"\nc:"+str(otrosCaracteres(cadena))

print(respuesta(input()))