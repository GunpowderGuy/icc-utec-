def noConsecutivos(cadena):
    ultimo = len(cadena)-1
    index = filter(lambda x : cadena[x] != cadena[x+1]  ,range(ultimo))
    return list(map(lambda x : cadena[x] ,index)) + list(cadena[ultimo])

palabras = lambda cadena : len(list(filter(lambda x : x == " ",cadena))) + 1 

otrosCaracteres = lambda cadena : len(cadena) - palabras(cadena) + 1

respuesta = lambda cadena : "p:"+str(palabras(noConsecutivos(cadena)))+"\nc:"+str(otrosCaracteres(cadena))

print(respuesta(input()))