#import functools as fun
import math

s = list(map(lambda x: pow(x,2) ,range(10)))

v = list(map(lambda x : pow(2,x) ,range(13)))

m = list(filter(lambda x : x % 2 , s))

palabras = ["uno","dos","tres"]

primero = list(map(lambda x : x[0],palabras ))

exactos = list(filter(lambda x : x == int(x) ,map(math.sqrt , range(1,101))))

def noConsecutivos(cadena):
    ultimo = len(cadena)-1
    index = filter(lambda x : cadena[x] != cadena[x+1]  ,range(ultimo))
    return list(map(lambda x : cadena[x] ,index)) + list(cadena[ultimo])

terna = map()

combinaciones = map(map(),range(4))

print(*noConsecutivos("aa"),sep=" ")

map(lambda x : list(map()),range(1,100))
