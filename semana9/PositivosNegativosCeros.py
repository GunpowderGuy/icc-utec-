# https://www.hackerrank.com/contests/semana09-jueves-s1-l6-sesion2-9amprof-mariano-melgar/challenges

def validInput():
    i = int(input())
    if i > 0:
        return i
    else:
        print("Invalid input")
        #exit()

def segregar(enteros):
    positivos = list(filter(lambda x : x > 0 ,enteros))
    negativos = list(filter(lambda x : x < 0 ,enteros))
    ceros = list(filter(lambda x : x == 0 , enteros ))
    return negativos + ceros + positivos 

print(*segregar(list(map(lambda x : int(input()) ,range(validInput())))),sep="\n")