from functools import reduce
import operator

def funcion(lista):
    filtrados = list(filter(lambda x : x >= 10 , lista))
    filtrados.extend([0,0])
    return reduce(operator.add , filtrados)

def validInput():
    n = int(input())
    
    if n > 0 :
        return n
    else :
        print("Invalid input")
        exit()
        
inlista = list(map(lambda x : int(input()) , range(validInput()))) 

def quinto(lista):
    if len(lista) >= 5 :
        return str(lista[5])
    else :
        return "No existe el elemento"
    
print(quinto(inlista))